# julia --sysimage /share/opt/julia/libIpaper.so --project scripts/s05_clusterInfo.jl

## 计算统计指标 -----------------------------------------------------------------
begin
  include("main_pkgs.jl")
  using DataFrames: DataFrame
  using Pkg
  Pkg.activate("/share/home/kong/github/jl-spatial/SpatioTemporalCluster.jl")
  using Revise
  using SpatioTemporalCluster


  f_grid = "/share/home/kong/github/jl-spatial/SpatioTemporalCluster.jl/data/China_GCM_050deg_FractionArea_V1.csv"
  @time d_coord = fread(f_grid) |> tidy_coord
end

# methods(cal_clusterInfo)
f_id = "/share/home/kong/github/rpkgs/rcdo/INPUT/clusterId_HItasmax_day_ACCESS-CM2_ssp126_r1i1p1f1_gn_20150101-21001231.nc"
# cal_clusterInfo(f_id; d_coord=d_coord, probs=[0.9, 0.95])


# f_id = "/share/home/kong/github/rpkgs/rcdo/INPUT/clusterId/hist-GHG/clusterId_HItasmax_day_FGOALS-g3_hist-GHG_r1i1p1f1_gn_18500101-20201231.nc"
fs_id = [
  # "clusterId_HItasmax_day_TaiESM1_piControl_r1i1p1f1_gn_02010101-07001231.nc"
  "clusterId_HItasmax_day_ACCESS-CM2_piControl_r1i1p1f1_gn_09500101-14491231.nc"
  "clusterId_HItasmax_day_CMCC-ESM2_piControl_r1i1p1f1_gn_18500101-23491231.nc"
  "clusterId_HItasmax_day_EC-Earth3-CC_piControl_r1i1p1f1_gr_18500101-23541231.nc"
  "clusterId_HItasmax_day_TaiESM1_piControl_r1i1p1f1_gn_02010101-07001231.nc"
]

root = "/share/home/kong/github/rpkgs/rcdo/INPUT/clusterId/piControl"
for f_id in fs_id
  f_id = "$root/$f_id"
  @show f_id
  @time cal_clusterInfo(f_id; d_coord=d_coord)
end
