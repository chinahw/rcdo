# julia --threads 16 test/Vogel2020/test_vogel2020.jl
using SpatioTemporalCluster
@time using Ipaper
using nctools
using JLD2


function post_process(clutserId, df; f_HI, f_mTRS, subfix=nothing)
  ## 2. SAVE DATA ----------------------------------------------------------------
  outdir = dirname(f_mTRS)
  if subfix === nothing
    subfix = str_replace(basename(f_mTRS), ".nc", "")
  end
  f_id = "$outdir/clusterId_$subfix.nc"

  _dims = nc_dims(f_HI)
  probs = [0.9, 0.95, 0.99, 0.999, 0.9999]
  dim_prob = NcDim("prob", probs)
  dims = [_dims[1:3]; dim_prob]
  @time nc_write(f_id, "clusterId", clutserId, dims; compress=1, overwrite=true)

  ## 3. 统计信息
  f_grid = "/share/home/kong/github/jl-spatial/SpatioTemporalCluster.jl/data/China_OBS_025deg_FractionArea.csv"
  d_coord = fread(f_grid) |> tidy_coord
  clusterId_df2csv(df, d_coord; outdir="OUTPUT/OBS", subfix)
end


function main_clusterId_full(arr, dates;
  f_HI, f_mTRS, T_wl=nothing, subfix=nothing, parallel=true)

  probs = PROBS
  @time mTRS_full = nc_read(f_mTRS, "mTRS"; ind=(:, :, :, :))

  # 每年最热的三个月，升温幅度；考虑升温幅度的快慢
  @time clutserId, df = STcluster_full(arr, dates, mTRS_full, probs; parallel, T_wl)
  post_process(clutserId, df; f_HI, f_mTRS, subfix)
end


function get_wl(f_wl; p1=1961, p2=1990)
  T_wl = nc_read(f_wl, "T_wl", type=Float32)
  itime = (p1:p2) .- 1960
  T_wl = T_wl .- mean(@view(T_wl[:, :, itime]), dims=3)

  list_wl = SortedDict{Int,AbstractArray}()
  for year = 1961:2021
    i = year - 1960
    list_wl[year] = T_wl[:, :, i]
  end
  list_wl
end

## 读取数据
f_HI = "/share/Data/CN0.5.1_ChinaDaily_025x025/HI-Tmax_CN05.1_1961_2021_daily_025x025.nc"

dates = nc_date(f_HI)
@time arr = nc_read(f_HI);
@time arr[arr.==-9999.0f0] .= NaN32;


## 1. `mTRS_base` and `mTRS_full` 实战
fs = [
  "/share/home/kong/github/rpkgs/rcdo/INPUT/OBS/OBS_mTRS_base_1961-1990.nc"
  "/share/home/kong/github/rpkgs/rcdo/INPUT/OBS/OBS_mTRS_base_1981-2010.nc"
  "/share/home/kong/github/rpkgs/rcdo/INPUT/OBS/OBS_mTRS_full.nc"
  "/share/home/kong/github/rpkgs/rcdo/INPUT/OBS/OBS_mTRS_full_simple.nc"
]
# f_mTRS = "/share/home/kong/github/rpkgs/rcdo/INPUT/OBS/OBS_mTRS_full.nc"
# dir("/share/home/kong/github/rpkgs/rcdo/INPUT/OBS", "OBS.*.nc")
nc_info(fs[1])
f_mTRS = fs[1]

if false
  for f_mTRS in fs
    @show f_mTRS
    main_clusterId_full(arr, dates; f_HI, f_mTRS)
  end
end

## 2. 考虑升温幅度
nc_info(f_wl)

f_mTRS = "/share/home/kong/github/rpkgs/rcdo/INPUT/OBS/OBS_mTRS_base_1961-1990.nc"
f_wl = "/share/home/kong/github/rpkgs/rcdo/INPUT/OBS/OBS_mTRS_season-T_wl.nc"
main_clusterId_full(arr, dates; f_HI, f_mTRS, T_wl, subfix="OBS_mTRS_season_1961-1990")


f_mTRS = "/share/home/kong/github/rpkgs/rcdo/INPUT/OBS/OBS_mTRS_base_1981-2010.nc"
T_wl = get_wl(f_wl; p1=1981, p2=2010) #list_wl
main_clusterId_full(arr, dates; f_HI, f_mTRS, T_wl, subfix="OBS_mTRS_season_1981-2010")

## 3. check mTRS_season
T_wl = get_wl(f_wl; p1=1961, p2=1990) #list_wl

d_wl = @pipe map(year -> begin
  mat = T_wl[year]
  wl = nanmean(mat)
  DataFrame(;year, T_wl=wl)
end, 1961:2021) |> melt_list(_)
mean(d_wl.T_wl[1:30])
