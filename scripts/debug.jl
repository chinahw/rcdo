include("main_pkgs.jl")
using DataFrames: DataFrame
using Pkg
Pkg.activate("~/github/jl-spatial/SpatioTemporalCluster.jl")
using Revise
using SpatioTemporalCluster


l = load("debug.jld2")
lst = l["lst"]
d_coord = l["d_coord"]

df = lst[2]
df2 = dt_merge(df, d_coord)

clusterId_df2csv(lst, d_coord)
