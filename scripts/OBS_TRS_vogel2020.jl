# julia --threads 16 test/Vogel2020/test_vogel2020.jl
using SpatioTemporalCluster
@time using Ipaper
using nctools
using JLD2

function _ncwrite(mTRS, outfile="OBS_mTRS_full.nc"; full=true)
  _dims = nc_dims(f)
  probs = [0.9, 0.95, 0.99, 0.999, 0.9999]
  dim_doy = NcDim("doy", 1:366)
  dim_prob = NcDim("prob", probs)
  dims = full ? [_dims; dim_prob] : [_dims[1:2]; dim_doy; dim_prob]

  @time nc_write(outfile, "mTRS", mTRS, dims; compress=1, overwrite=true)
  # jldsave("OBS_mTRS_full.jld2"; mTRS, lon, lat, dates, compress=true)
end


f = "/share/Data/CN0.5.1_ChinaDaily_025x025/HI-Tmax_CN05.1_1961_2021_daily_025x025.nc"

dates = nc_date(f)
@time arr = nc_read(f);
@time arr[arr.==-9999.0f0] .= NaN32;

# Range(dates)
# size(arr)
# length(dates)
# dates[[1, end]]

## 1. mTRS 1961-1990
@time mTRS = cal_mTRS_base(arr, dates; na_rm=true, p1=1961, p2=1990);
_ncwrite(mTRS, "OBS_mTRS_base_1961-1990.nc"; full=false)
# rm("OBS_mTRS_base_1961-1990.nc")

@time mTRS = cal_mTRS_base(arr, dates; na_rm=true, p1=1981, p2=2010);
_ncwrite(mTRS, "OBS_mTRS_base_1981-2010.nc"; full=false)


# 2. mTRS_season
@time T_wl = cal_mTRS_season(arr, dates);

_dims = nc_dims(f)
probs = [0.9, 0.95, 0.99, 0.999, 0.9999]
dim_year = NcDim("year", 1961:2021)
dims = [_dims[1:2]; dim_year]
@time nc_write("INPUT/OBS/OBS_mTRS_season-T_wl.nc", "T_wl", T_wl, dims; compress=1, overwrite=true)

## 3. mTRS_full
# `mTRS_full`最耗时
Threads.nthreads()

if false
  @time mTRS_full = cal_mTRS_full(arr, dates; use_mov=true, na_rm=true)
  _ncwrite(mTRS_full, "OBS_mTRS_full_V2.nc")
end

# 1675.866281 seconds (8.29 G allocations: 3.986 TiB, 24.24% gc time, 0.45% compilation time)
# 1376.109929 seconds (5.21 G allocations: 334.383 GiB, 24.46% gc time, 1.52% compilation time)
# 0.465 hours, 16 threads

# @profview_allocs mTRS_full = cal_mTRS_full(arr, dates; probs=[0.9]);
@time mTRS_full_simple = cal_mTRS_full(arr, dates; use_mov=false, na_rm=true);
_ncwrite(mTRS_full_simple, "OBS_mTRS_full_simple_V2.nc")

## 验收结果
# `full`和`simple`差距还是挺明显
